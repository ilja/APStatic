# APStatic
## FAQ
### What?

[Activity Pub](https://www.w3.org/TR/activitypub/) allows us to have a global connected social network, no longer are you stuck to one platform or provider. With this (really just for fun) project, I want to provide the building blocks to have a static site connected to [the fediverse](https://en.wikipedia.org/wiki/Fediverse) over Activity Pub.

### Fedi what? Activity who?

When you're with a telecom provider you can call your friends, even if they are with another provider. We consider this normal, but this isn't necessarily the case. If I take two empty cans and connect them with a wire, I basically create my own tin can telephone network. Yet, my friends who are on the actual telephone network wont be able to contact me.

When we look at the typical current day online social networks, we notice they don't behave like a phone network at all! If you want to connect to someone, you'll need an account on the specific platform they are on! In that sense, these platforms are really nothing more but a tin can telephone network.

Luckily there is an actual telephone network and luckily there is an actual social network. The different providers that make up the network have a way of talking to each other, and this communication makes it possible for you to connect to your friends, even if they are with a different provider. Maybe it all sounds like occult knowledge, but the fact is that it works.

Now what's the fediverse and Activity Pub you still wonder? Well, the fediverse, or just "fedi" for short, is a social network where people can talk to each other, even if they are on different platforms. Activity Pub is the way the different platforms talk to each other, allowing you to connect to your friends.

### Sounds cool. But I still don't have a clue what this repo is about.

The idea is to have a static blog and federate over Activity Pub. In Activity Pub you'll normally push content out to others. However, it's also possible for others to pull things in as well. The idea is to have a static blog whose articles can be pulled in by other instances. That way you can interact with it, and through your interactions, your article can spread across the fediverse.

This repository is not a software you install. It's more of a resource to help you understand enough of the building blocks you need to build your own static blog.

The first time I shared this idea was <https://ilja.space/notice/ADfYpziFRczzP7UxjE>. I'm not the first to come up with this idea. A certain Jan-Lukas already has [a static blog using Hugo that federates over Activity Pub](https://jlelse.blog/dev/activitystreams-hugo).

## What we need

Let's say we want to publish an article. We can represent that with Activity Pub. You'll want to attribute it to someone. That someone is represented as an actor. You can even make separate blogs and represent that in AP by using different actors. An Actor needs an inbox and outbox, and should have a followers and following collection. You also have to make sure that your activities have the correct `content-type`.

It should be noted that if we want to federate, we'll need to remain compatible with other software. Different implementations may each have their own quirks. Some are much looser in what they accept, others are more strict.

### The specifications

First of all there's the [Activity Pub](https://www.w3.org/TR/activitypub/) specification. It uses [Activity Streams](https://www.w3.org/TR/activitystreams-core/), which has it's own [Activity Streams Vocabulary](https://www.w3.org/TR/activitystreams-vocabulary). The vocabulary is how things are represented and use [JSON-LD](https://www.w3.org/TR/json-ld/) as a format.

You don't need to read all of the specifications, but if you want to have a better understanding of Activity Pub, reading them is not a bad idea. Note that reading them won't be enough to federate with the rest of the fediverse. There are still many holes left open, which implementors had to fill themselves. In the end, the network works because people work on it and try to remain somewhat compatible with each other.

### The Actor

One way to represent a person (i.e. yourself as an author of a series of articles) is to have an actor. The Activity Pub specs tells us the following about [the actor](https://www.w3.org/TR/activitypub/#actor-objects):

> Actor objects MUST have, in addition to the properties mandated by 3.1 Object Identifiers, the following properties:
>
> inbox
>    A reference to an [ActivityStreams] OrderedCollection comprised of all the messages received by the actor; see 5.2 Inbox. 
> outbox
>    An [ActivityStreams] OrderedCollection comprised of all the messages produced by the actor; see 5.1 Outbox. 

On Object Identifiers, it tells us

> All objects have the following properties:
>
> id
>     The object's unique global identifier (unless the object is transient, in which case the id MAY be omitted). 
> type
>     The type of the object. 

About identifiers, we read

> These identifiers must fall into one of the following groups:
> 
>     1. Publicly dereferencable URIs, such as HTTPS URIs, with their authority belonging to that of their originating server. (Publicly facing content SHOULD use HTTPS URIs).
>     2. An ID explicitly specified as the JSON null object, which implies an anonymous object (a part of its parent context)

So we need an id, type, and inbox and outbox. And we need to make sure they all have dereferencable URIs (aka, they need to be url's that host the json and we need to be able to fetch that json). And since it's JSON-LD, we also need a `@context`. AP uses `https://www.w3.org/ns/activitystreams` [as @context](https://www.w3.org/TR/activitypub/#obj).

We can check the Activity Streams Vocabulary [to see the different Actor Types](https://www.w3.org/TR/activitystreams-vocabulary/#actor-types). Here we see there's a Type Person which "Represents an individual person". Seems the right choice for us!

Since the id's need to be dereferencable, we'll need a location where we can host the files. Let's say we use the domain `example.me` and put the representation of our user (aka the Actor) in `https://example.me/users/ilja.json`. We'll put the inbox and outbox on `https://example.me/users/ilja/inbox.json` and `https://example.me/users/ilja/outbox.json` respectively. 

Let's put all of it in an object

```json
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "id": "https://example.me/users/ilja.json",
  "type": "Person",
  "inbox": "https://example.me/users/ilja/inbox.json",
  "outbox": "https://example.me/users/ilja/outbox.json"
} 
```

Note that implementations may expect more properties. Check the [Activity Pub specification for Actors](https://www.w3.org/TR/activitypub/#actor-objects) to see what more can be added.

If you want to fetch an actor from an existing implementation (e.g. because you want to use it as a cheat sheet, or just because you want to know how it looks like), you can do that with e.g. curl. In many cases you can just use the url you use to go to the person's profile. Let's say that url is `https://some-instance.fedi/users/me`, then you can fetch the object with

```sh
curl -L "https://some-instance.fedi/users/me"  -H 'Accept: application/ld+json; profile="https://www.w3.org/ns/activitystreams"'
```

Here's an example of an actor that has more properties. We used `.json` where we reference other Activity Objects. If you add them to the actor, make sure the object exists at the correct location.

```json
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "id": "https://example.me/users/ilja.json",
  "type": "Person",
  "inbox": "https://example.me/users/ilja/inbox.json",
  "outbox": "https://example.me/users/ilja/outbox.json",
  "url": "https://example.me/users/ilja",
  "name": "Ilja",
  "summary": "Hewoo, I write, I hope you like my articles <3",
  "icon": "https://example.me/media/pfp_ilja.jpeg",
  "following": "https://example.me/users/ilja/following.json",
  "followers": "https://example.me/users/ilja/followers.json",
  "liked": "https://example.me/users/ilja/liked.json",
  "streams": "https://example.me/users/ilja/streams.json",
  "preferredUsername": "Ilja",
  "endpoints": {
    "sharedInbox": "https://example.me/sharedInbox.json"
  }
} 
```

### The Collections

Activity Streams has the concept of Collections. They are a list of objects. The inbox and outbox are both Collections. Here we use an OrderedCollection with zero content.

This is for the Inbox

```json
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "id": "https://example.me/users/ilja/inbox.json",
  "type": "OrderedCollection",
  "totalItems": 0,
  "orderedItems": []
}
```

And here for the Outbox. Note that we have an item already. This item will be the `id` of our Article. Also note the `totalItems` property. When adding a new article, you should add it in the `orderedItems` and increment the `totalItems`. Things probably wont break if you do this wrong, though. It's just correct to do.

```json
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "id": "https://example.me/users/ilja/outbox.json",
  "type": "OrderedCollection",
  "totalItems": 1,
  "orderedItems": ["https://example.me/articles/vegan-choccy-cheese.json"]
}
```

### The Article

Now we can build a simple Article Object. We assume the human readable article will be hosted on `https://example.me/articles/vegan-choccy-cheese`.

```json
{
  "@context": "https://www.w3.org/ns/activitystreams",
  "id": "https://example.me/articles/vegan-choccy-cheese.json",
  "type": "Article",
  "attributedTo": "https://example.me/users/ilja.json",
  "url": "https://example.me/articles/vegan-choccy-cheese",
  "name": "Vegan Choccy Cheese",
  "summary": "Why vegan choccy cheese is great",
  "content": "<p>Chockolate is nommy.</p><p>Good self made vegan cheese is often very nommy too.</p><p>Therefore vegan choccy cheese must be very nomnom too!</p><p>I never actually had any (--) but I really want to (^^) thx you for your support and reading my blog post (^.-)~*</p>",
  "published": "2022-09-14T18:53:00.000Z",
  "to": ["https://www.w3.org/ns/activitystreams#Public"],
  "cc": [],
  "attachment": [],
  "tag": []
}
```

There's more you can add. If you see articles that have things you want (like tags for example), use curl in the same way we showed before too see how it looks like in the wild.

### Web server

When we [fetch an Object](https://www.w3.org/TR/activitypub/#retrieving-objects), we use `application/ld+json; profile="https://www.w3.org/ns/activitystreams"` [for content type and/or accept header](https://www.w3.org/TR/activitypub/#server-to-server-interactions). This is to be treated as equivalent to `content-type: application/activity+json`. (If one of the two doesn't work, try the other. If that one works, you found a bug in the software you're trying to pull from. Please file it on the correct issue tracker.)

When someone fetches something from our blog, they will go through a web server. We can make sure the correct responses are provided through that.

### NGINX

Since we used `.json` as extensions, we can add the following to NGINX to make it return the json objects as `application/ld+json; profile="https://www.w3.org/ns/activitystreams"`.

```nginx
location / {
    # Path to source
    alias /var/www/my_blog/;

    location ~ .*\.json {
     types { } default_type 'application/ld+json; profile="https://www.w3.org/ns/activitystreams"';
    }
}
```

## Getting it to federate

Alright! If everything is set up, we should now be able to fetch the article through it's `id`. You can use the curl command like before as a test. If it works, try searching it from the fedi server where you have an account. You should find it and be able to repeat, like, comment... Note that the static blog wont know of the interactions, that's a limitation we currently have. But at least it federates and that was the goal of this!

## Going further

Some possibly interesting ideas that fit within the scope of this project:

* We currently can't pull in the article by it's url. When someone requests with e.g. `application/activity+json`, we should redirect to the id (in this case by adding .json to the url). You could even have the url and id be the same and give the correct response based on the accept header.
* Make it so that when people reply to it, your fedi account will also be mentioned by default. (I think adding the `id` of your own fedi in `to` should work.)
* You may want to write .md (or other) files and then have a build script to create the .html files and objects and put everything in the correct place.
* When we have a build script, maybe we can have it deliver activities through s2s AP?
* We can Save requests (at least log date, path and body) through the web server's logs (access_logs in nginx) and process them using the build script.
    * We can have the build script process follow requests. Maybe follow requests can be answered automatically by rewriting the request and "proxy_pass" it back to the originating server.
    * Other Activities could also be processed, but remember that they will only be processed when the script runs. You can add a cronjob or something of course. The question is how far you want to take it before having a not-so-static solution any more ;)

## Now what?

Well that's it :) I hope it's useful for someone. I'm not sure if this is something I'll work on more, but maybe. I'm sure it still lacks a lot. It may need better structure, some things are maybe not explained clearly enough, I would like more and better examples for the reverse proxy (especially Caddy)...

Feel free to make issues, PR's and/or forks, and we'll see what happens!

## License

Except where otherwise noted, content of this text is licensed under a [Creative Commons Attribution 4.0 International license](https://creativecommons.org/licenses/by-sa/4.0/) license.
